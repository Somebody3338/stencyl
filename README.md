<!-- Change the title of the class -->

# Stencyl

---

### Game Design
We're going to create a space battle video game!  Our game will leverage a combination of premade behaviors provided by Stencyl as well as a custom behavior that we'll make ourselves.

---

### Requirements:
  - You can use a Mac, Windows, Linux, or Chromebook computer.
  - You can run Stencyl on your own Mac, Windows, or Linux computer. If you're going to go this route, please make sure you have the app installed prior to class.  [Download here](http://www.stencyl.com/download/)
  - Alternatively, SFS can provide remote workstations with Stencyl preinstalled. If you need this, please let us know in advance. And please install Microsoft Remote Desktop on your Mac, Windows, or Linux computer prior to class. If you're using Linux, we recommend Remmina.
  - Chromebooks must use the remote workstation option, so please install the [Microsoft Remote Desktop app from the Play Store](https://play.google.com/store/apps/details?id=com.microsoft.rdc.android&hl=en_US) prior to class. 

---

### Environment Setup

Make a `game_assets` folder somewhere on your computer.  Use this to save images and other game data.

---

# Labs

<!-- OPTIONAL: Create a Table of Contents. -->
<!-- Make sure to keep this up to date as you make edits. -->
<!-- If you opt not to do this, a link to the first lab is helpful. -->

### 0. [Getting Started](labs/00_setup/Readme.md)

### 1. [Adding Pre-Made Behaviors](labs/01_adding_premade_behaviors/Readme.md)

### 2. [Writing Your Own Behavior](labs/02_writing_your_own_behavior/Readme.md)
