<!-- Update the lab title -->
# Starting Your Game

<!-- Reminder: -->
<!-- Labs should take ~60 minutes -->
<!-- ~25% Talk: What and why. -->
<!-- ~25% Demo: What and how. -->
<!-- ~50% Do: Pair and share, redoing the demo -->

<!-- IF THIS IS THE LAST LAB, PUT FEEDBACK REMINDERS AT THE START -->

<!-- Update this sub-header -->
### Getting Your Game Ready

<!-- 3-4 sentences -->
<!-- Answer the following questions -->
- In this lab, we will set up our game, by adding actors and backgrounds, to create the scene for the level.
- This will set up the game to be created. By the end of the lab, the characters will be getting ready to start doing something.

---

<!-- Update this section name -->
### Creating A Game And Adding Actors (Characters in your game)

<!--
As you demo...
  explain what you're doing in each step
  explain what's happening in each step
If relevant...
  explain where each step fits into the overall workflow
  explain where the action is happening (e.g. local vs. remote web server)
-->

1. Open Stencyl, and click `Create Game`
2. Download the files from this project to your `game_assets` folder.

Create Actors
- Player UFO
- Enemy UFO
- Blaster
- Enemy Bullet

For each actor listed above:
1. Go to the `Dashboard` tab
2. Select the `Actor Types` resource
3. Click `Create New`
4. Give it a name
5. Click the large box in the middle of the screen: `Click here to add an animation`
6. Under `Frames`, click `Click here to add a frame`
7. Click `Choose image`
  - Note: For the UFO's and enemy bullet, the `Scale` should be `4x`
  - For the Blaster, change the `Scale` to `1x`
8. Click `Add`

---

### Create a Background

1. Go to the `Dashboard` tab
2. Select the `Backgrounds` resource
1. Click `Create New`
1. Name it `star bg`
1. Click `Click here to add a frame`
1. Click `Choose Image`
2. Select `star_bg.jpg`
1. Click `Add`
3. This will take you to the tab for that background. Click the box next to `Repeat Background`.
4. Next, save your game, using the icon at the top.

### Build the Scene

1. Go to the `Dashboard` tab
2. Select the `Scenes` resource
1. Click `Create New`
1. Name it `Level One`
1. Change the size to `640 pixels` x `480 pixels`
1. Click `Create`

### Attach the Background

1. Go to your `star bg` tab
1. Click the `Attach to Scene` button
1. Select `Level One`
1. Click `OK`

### Add the UFO's

1. Go to your `player ufo` tab
1. Click `Add to scene`
1. Select `Level One`
1. Click `OK`
1. Click on the right side of the screen to place the UFO
  - If you don't see the UFO, check the `Layers` pane in the bottom right corner of the app
  - Drag the `star bg` layer to the bottom
1. Repeat for the `enemy ufo`, but put this one on the left side of the screen.



Now your game should be ready to start working on!
