<!-- Update the lab title -->
# Writing Your Own Behavior

### What Are We Doing?

- In this lab, we are going to write our own collisions behavior.
- This will complete the game. In the process we will give each UFO a set amount of lives, and one will die when it reaches 0.

---

### Life Attributes

1. Go to `Settings`
1. Go to the `Attributes` tab

##### Player Lives
1. Click `Create New`
1. Call this `player lives`
1. Make the category numbers
1. Choose a number from 3-5

##### Enemy Lives
1. Click `Create New`
1. Call this `enemy lives`
1. Make the category numbers
1. Choose a number from 7-10


Close settings with the `OK` button and save your game!

---

### Enable Collisions

1. Go to the `Player UFO` tab
1. Go to the `Collisions` sub-tab
1. Check the box next to `Is a sensor?`
1. Go to the `Enemy UFO` tab
1. Go to the `Collisions` sub-tab
1. Check the box next to `Is a sensor?`

---

### Collisions For The Player UFO

![Behavior code](behavior.png)

1. Go to `Dashboard`
1. Go to the `Scene Behaviors` sub-tab
1. Click `Create New Behavior`
1. Name it `Hits`
1. Click `Create`

#### Making an Event

1. Click `add event`
1. Hover over `collisions`, then click `Type & Type`
1. Right-click the `Type - Type` event
1. Rename the event to `Player UFO`
1. In the `when` block, set the first actor to `enemy bullet`, and the second to `player ufo`

#### Managing Player Lives
1. Search for the `set Player Lives to ...` block (in the `Palette` pane on the right)
1. Drag the `set Player Lives to ...` block inside the top of the `when` block
1. Click the drop-down in `set player lives to`
1. Hover over `Math`
1. Click on the green block with the two spaces and the `[dropdown] - [dropdown]`
1. Use the drop-down to select `Game Attributes` > `player lives` on the left side of the block
1. Type `1` (the number one) in the right side of the block

### Destroy the bullet
1. Add a `kill [actor]` block
1. Drag the `1st actor` block (from the `when` block) over the `[actor]` in the `kill` block

#### Check the Player Lives
1. Add an `if` block
1. Select `Comparison` and the `<=` option
1. Set the first drop-down to `Game Attributes` > `player lives`
1. Type `0` (the number zero) into the second space

### Kill the Player
1. Add a `kill [actor]` block inside your `if` block
1. Drag the `2nd actor` block (from the `when` block) over the `[actor]` in the `kill` block

---

### Collisions For The Enemy UFO

1. Right-click on the `Player UFO` event
2. Click `duplicate`
1. Right-click the `Player UFO copy` event
1. Rename the event to `Enemy UFO`
3. Switch out each block that says `player UFO` to the same block for the `enemy UFO`.
4. Switch `enemy bullet` to `blaster`

![Enemy hit](enemy_hit.png)

---

### Attaching These Behaviors

1. Click the `Attach to Scene` button
2. Select `Level One`
3. Try your game!
