# Adding Premade Behaviors

### What Are We Doing?

- In this lab, we will be adding premade behaviors so that our actors can do things like move.
- This will set a basic game so that we can make a collisions behavior.

---

### Adding Behaviors To The Player UFO

1. Go to your `player ufo` tab
1. Select the `Behaviors` sub-tab
1. Click `+ Add Behavior`
2. Add these behaviors:
  - `Motion`: `Cannot Exit Screen`
  - `Controls`: `4 Way Movement`
  - `Controls`: `Fire Bullet`
3. Save your game.

---

### Adding Behaviors To The Enemy UFO

1. Go to the `enemy UFO` tab
1. Select the `Behaviors` sub-tab
1. Click `+ Add Behavior`
2. Add these behaviors:
  - `Motion`: `Cannot Exit Screen`
  - `Motion`: `Back and Forth Vertically`
  - `Controls`: `Fire Bullet Wave`
3. Save your game.

---

### Configure Enemy UFO Behaviors
1. Go to the `enemy UFO` tab
1. Select the `Behaviors` sub-tab
3. Go to `back and forth vertically`, and make distance up 100, and the same for distance down.
4. Go to `settings`>`controls`>`add control`, and add `spacebar` as a control.
5. Configure `Fire Bullet Wave`:
  - It should use the `space` control
  - `bullet type` should be the `enemy bullet`
  - Number of bullets: `3.0`
  - Bullet spread: `30`
  - Bullet speed: `20`
  - Direction mode: `relative`
  - Direction: `east`

---

### Configure Player UFO Behaviors

1. Go to the `player UFO` tab.
2. Configure the `four way movement` behavior
  - Use the controls `up`, `down`, `left`, `right`
  - Speed: `30`
  - Set all of the animations to your only player UFO animation.
3. Configure `Fire Bullet`
  - Make fire control `space`
  - Bullet type: `blaster`
  - Bullet speed: `50`
  - Direction mode: `absolute`
  - Direction: `west`
4. Save your game.
